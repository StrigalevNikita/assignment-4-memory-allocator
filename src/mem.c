#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define MAP_CONST 0x100000

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t new_reg_actual_size = region_actual_size(size_from_capacity((block_capacity) {.bytes = query}).bytes);
  void* new_reg_adress_map = map_pages(addr, new_reg_actual_size, MAP_CONST);

  if (new_reg_adress_map == MAP_FAILED) {
       	new_reg_adress_map = map_pages(addr, new_reg_actual_size, 0);
  }
  if (new_reg_adress_map == MAP_FAILED) {
      	return REGION_INVALID;
  }

  block_init(new_reg_adress_map, (block_size) {.bytes = new_reg_actual_size}, NULL);
  return (struct region) {.size = new_reg_actual_size, .addr = new_reg_adress_map, .extends = (addr == new_reg_adress_map)};
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
  if ((block == NULL) || (!block_splittable(block, query))){
	return false;
  }
  else{
	void* another_block = query + block -> contents;
	block_size another_block_size = {.bytes = (block -> capacity.bytes) - query};
	block_init(another_block, another_block_size, block -> next);
	block -> next = another_block;
	block -> capacity.bytes = query;
	return true;
  }
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if(block -> next == NULL || !mergeable(block, block -> next)){
        return false;
  }
  block -> capacity.bytes += size_from_capacity(block -> next -> capacity).bytes;
  block -> next = block -> next -> next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last (struct block_header* restrict block, size_t sz)    {
  if (block == NULL){
	return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
  }
  struct block_search_result answer = (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
  while (block){
	while (try_merge_with_next(block));
	answer.block = block;
	if (block_is_big_enough(sz, block) && block -> is_free){
		answer.type = BSR_FOUND_GOOD_BLOCK;
		break; 
	}
	block = block -> next;
  }
  return answer;
}

static struct block_search_result try_memalloc_existing (size_t query, struct block_header* block) {
	struct block_search_result answer = find_good_or_last(block, query);
    	if (answer.type == BSR_FOUND_GOOD_BLOCK) {
        	split_if_too_big(answer.block, query);
        	answer.block -> is_free = false;
    	}

    	return answer;
}



static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
	if (!last){
		return NULL;
	}
	struct region new_region_allocated = alloc_region(block_after(last), query);
    	if (region_is_invalid(&new_region_allocated)){
        	return NULL;
	}

    	if (new_region_allocated.extends && last -> is_free) {
        	last -> capacity.bytes += new_region_allocated.size;
        	return last;
    	}
	    else {
    		  last -> next = new_region_allocated.addr;
    	  	return new_region_allocated.addr;
	}
}


static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
  if (query <= BLOCK_MIN_CAPACITY){
		  query = BLOCK_MIN_CAPACITY;
	}
	struct block_search_result answer = try_memalloc_existing(query, heap_start);
    	if (answer.type == BSR_REACHED_END_NOT_FOUND) {
        	struct block_header *new_reg_allocated = grow_heap(answer.block, query);
        	answer = try_memalloc_existing(query, new_reg_allocated);
    	}

    	if (answer.type == BSR_CORRUPTED) {
        	return NULL;
    	}
    	return answer.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header -> is_free = true;
  while (try_merge_with_next(header));
}
