#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define HEAP_SIZE 12345
#define BLOCK_SIZE 123
#define MAP_CONST 0x100000

void print_string(const char* const message){
	fprintf(stdout, "%s", message);
}

void print_exception(const char* const exception){
	fprintf(stderr, "%s", exception);
}

void test_one_allocation(){
	print_string("=====Test1=====\n");
    	void* normal_heap = heap_init(HEAP_SIZE);
    	if (!normal_heap){
        	print_exception("Can't create heap\n");
    	}
    	debug_heap(stdout, normal_heap);
    	void* block1 = _malloc(BLOCK_SIZE);
	print_string("Allocated one block:\n");
	debug_heap(stdout, normal_heap);
	_free(block1);
	print_string("Free one block:\n");
    	debug_heap(stdout, normal_heap);
    	munmap(normal_heap, size_from_capacity((block_capacity) {.bytes = HEAP_SIZE}).bytes);
    	printf("=====Test1 ok=====\n\n\n");
}

void test_free_one(){
	print_string("=====Test2=====\n");
    	void* normal_heap = heap_init(HEAP_SIZE);
    	if (!normal_heap){
        	print_exception("Can't create heap\n");
    	}
    	debug_heap(stdout, normal_heap);
    	void* block1 = _malloc(BLOCK_SIZE);
	void* block2 = _malloc(BLOCK_SIZE * 2);
	void* block3 = _malloc(BLOCK_SIZE * 3);
	print_string("Allocated three blocks:\n");
	debug_heap(stdout, normal_heap);
	_free(block2);
	print_string("Free second block:\n");
    	debug_heap(stdout, normal_heap);
	_free(block1);
	_free(block3);
	print_string("Free last blocks:\n");
    	debug_heap(stdout, normal_heap);
    	munmap(normal_heap, size_from_capacity((block_capacity) {.bytes = HEAP_SIZE}).bytes);
    	printf("=====Test2 ok=====\n\n\n");
}

void test_free_two(){
	print_string("=====Test3=====\n");
    	void* normal_heap = heap_init(HEAP_SIZE);
    	if (!normal_heap){
        	print_exception("Can't create heap\n");
    	}
    	debug_heap(stdout, normal_heap);
    	void* block1 = _malloc(BLOCK_SIZE);
	void* block2 = _malloc(BLOCK_SIZE * 2);
	void* block3 = _malloc(BLOCK_SIZE * 3);
	print_string("Allocated three blocks:\n");
	debug_heap(stdout, normal_heap);
	_free(block1);
	print_string("Free first block:\n");
    	debug_heap(stdout, normal_heap);
	_free(block3);
	print_string("Free third block:\n");
    	debug_heap(stdout, normal_heap);
	_free(block2);
	print_string("Free last block:\n");
    	debug_heap(stdout, normal_heap);
    	munmap(normal_heap, size_from_capacity((block_capacity) {.bytes = HEAP_SIZE}).bytes);
    	printf("=====Test3 ok=====\n\n\n");
}


void test_memory_limit(){
	print_string("=====Test4=====\n");
    	void* normal_heap = heap_init(HEAP_SIZE);
    	if (!normal_heap){
        	print_exception("Can't create heap\n");
    	}
    	debug_heap(stdout, normal_heap);
	print_string("Allocate block more than heap\n");
    	void* block1 = _malloc(HEAP_SIZE * 2);
	debug_heap(stdout, normal_heap);
	print_string("Free one block:\n");
	_free(block1);
    	munmap(normal_heap, size_from_capacity((block_capacity) {.bytes = HEAP_SIZE}).bytes);
    	printf("=====Test4 ok=====\n\n\n");
}

void test_region_memory_limit(){
	print_string("=====Test5=====\n");
    	void* normal_heap = heap_init(HEAP_SIZE);
    	if (!normal_heap){
        	print_exception("Can't create heap\n");
    	}
    	debug_heap(stdout, normal_heap);
	void* memory_add = mmap(normal_heap + REGION_MIN_SIZE * 2, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_CONST, -1, 0);
	print_string("Allocate memory\n");
	debug_heap(stdout, normal_heap);
	void* block1 = _malloc(HEAP_SIZE * 2);
	print_string("Allocate block with heap size * 2\n");
	debug_heap(stdout, normal_heap);
	_free(block1);
	print_string("Free one block\n");
	debug_heap(stdout, normal_heap);
    	munmap(normal_heap, size_from_capacity((block_capacity) {.bytes = HEAP_SIZE}).bytes);
	munmap(memory_add, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
	print_string("Free memory\n");
    	printf("=====Test5 ok=====\n\n\n");
}




int main() {
	print_string("Tests:\n");
    	test_one_allocation();
	test_free_one();
	test_free_two();
	test_memory_limit();
	test_region_memory_limit();
}
